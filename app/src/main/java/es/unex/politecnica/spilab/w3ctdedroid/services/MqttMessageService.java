package es.unex.politecnica.spilab.w3ctdedroid.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import es.unex.politecnica.spilab.w3ctdedroid.Constants;
import es.unex.politecnica.spilab.w3ctdedroid.MainActivity;
import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.fragments.ConfigFragment;
import es.unex.politecnica.spilab.w3ctdedroid.fragments.WSituationsFragment;
import es.unex.politecnica.spilab.w3ctdedroid.models.ProfileW3CTDE;

public class MqttMessageService extends Service {

    private static final String TAG = "MqttMessageService";
    private PahoMqttClient pahoMqttClient;
    private MqttAndroidClient mqttAndroidClient;
    //public static Boolean subscribed = false;

    private String profile = Constants.DEFAULT_PROFILE;
    private String profileW3CTD = Constants.DEFAULT_PROFILEW3CTD;

    private ProfileW3CTDE fullProfileW3CTDE = new ProfileW3CTDE();

    public MqttMessageService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startMyOwnForeground();
        else
            startForeground(1, new Notification());

        pahoMqttClient = new PahoMqttClient();

        readFileExternalStorageW3CTDE();

        //System.out.println(" - NOVA: " + fullProfileW3CTDE);

        if (fullProfileW3CTDE != null) {
            System.out.println(" - NOVA ENTRO");
            if (fullProfileW3CTDE.getTitle() != "") {
                //System.out.println(" - NOVA ENTRO11");

                //Constants.CLIENT_ID = MainActivity.fullProfile.getInfos().get(3).getValue();
                Constants.CLIENT_ID = fullProfileW3CTDE.getMacAddress();

                //Log.d(TAG, " - NOVA ID=" + Constants.CLIENT_ID);

                try {
                    pahoMqttClient.disconnect(mqttAndroidClient);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mqttAndroidClient = pahoMqttClient.getMqttClient(getApplicationContext(), Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);

                //System.out.println(" - NOVA;" + mqttAndroidClient.isConnected());

                mqttAndroidClient.setCallback(new MqttCallbackExtended() {
                    @Override
                    public void connectComplete(boolean b, String s) {
                        //System.out.println(" - NOVA SUBSSSS");
                        subscribeTopic(getApplicationContext(), "request");
                        subscribeTopic(getApplicationContext(), "situation");
                        //Log.d(TAG, "Subscribed to request");
                        //Log.d(TAG, "Subscribed to situation");

                    }

                    @Override
                    public void connectionLost(Throwable throwable) {
                        Log.d(TAG, "Service connection lost");
                        subscribeTopic(getApplicationContext(), "request");
                        subscribeTopic(getApplicationContext(), "situation");
                    }

                    @Override
                    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                        //Log.d(TAG, " - Message!!11=" + new String(mqttMessage.getPayload()));
                        //Log.d(TAG, "Prof=" + profile);

                        //setMessageNotification(s, new String(mqttMessage.getPayload()));

                        Log.d(TAG, " - Received" + s);

                        switch (s) {

                            case "request":
                                // Parse message
                                String msg = new String(mqttMessage.getPayload());
                                JSONObject json = new JSONObject(msg);

                                System.out.println(" - REQ=" + json);

                                // Check if I have to send the profile
                                //JSONObject jsonProfile = new JSONObject(profile);
                                JSONObject jsonProfile = new JSONObject(MainActivity.profileW3CTD);
                                String myName = jsonProfile.getString("id");
                                String reqName = json.getString("id");

                                if (json.has("macAddress")) {

                                    File f = new File(Environment.getExternalStorageDirectory(), "W3CTDE-Droid");
                                    readFileExternalStorage(f);

                                    Log.d(TAG, "REQ: Checking name and mac address...");
                                    String myMacAddress = getMacAddressUpdated();
                                    String reqMacAddress = json.getString("macAddress");

                                    Log.d(TAG, " - REQ: Requested: " + reqName + "," + reqMacAddress + "-> I am: " + myName + "," + myMacAddress);
                                    if (myName.equals(reqName) || myMacAddress.equals(reqMacAddress)) {
                                        Log.d(TAG, " - REQ: Its me!! (name or mac address -wifi-)");
                                        //sendProfile();
                                        ConfigFragment.sendProfile(getApplicationContext());
                                        setMessageNotification(s, new String("Profile requested"));
                                    } else {
                                        Log.d(TAG, " - REQ: Nothing to do");
                                    }

                                } else {

                                    File f = new File(Environment.getExternalStorageDirectory(), "W3CTDE-Droid");
                                    readFileExternalStorage(f);

                                    Log.d(TAG, "REQ: Checking name...");
                                    Log.d(TAG, " REQ: - Requested: " + reqName + "-> I am: " + myName);
                                    if (myName.equals(reqName)) {
                                        Log.d(TAG, " - REQ: Its me!! (name)");
                                        //sendProfile();
                                        ConfigFragment.sendProfileW3CTDE(getApplicationContext());
                                    } else {
                                        Log.d(TAG, " - REQ: Nothing to do");
                                    }

                                }
                                break;
                            case "situation":
                                Log.d(TAG, "Situation = " + mqttMessage.toString());

                                // Adding situation the the profile
                                try {
                                    JSONObject jsonSit = new JSONObject(mqttMessage.toString());
                                    Log.d(TAG, jsonSit.toString());

                                    Iterator<String> iter = jsonSit.keys();
                                    while (iter.hasNext()) {
                                        String key = iter.next();
                                        try {
                                            Object value = jsonSit.get(key);
                                            System.out.println(" - JSK=" + value);

                                            JSONObject jsValue = new JSONObject(value.toString());
                                            String jsId = jsValue.getString("id");

                                            Gson gson = new Gson();
                                            JsonObject js = gson.fromJson(value.toString(), JsonObject.class);

                                            System.out.println(" - JSI=" + jsId);
                                            System.out.println(" - JSC=" + js);

                                            MainActivity.fullProfileW3CTDE.getSituations().get(0).remove(jsId);
                                            MainActivity.fullProfileW3CTDE.getSituations().get(0).add(jsId, js);

                                            WSituationsFragment.reloadSituations();

                                            Snackbar.make(MainActivity.v, "Received new Situation: " + jsId, Snackbar.LENGTH_LONG)
                                                    .setAction("Action", null).show();

                                            MainActivity.writeFileExternalStorageW3CTDE();

                                        } catch (JSONException e) {
                                            // Something went wrong!
                                        }
                                    }


                                    //MainActivity.fullProfileW3CTDE.getSituations().add(js);


                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }


                                break;

                        }



                /*
                // Send profile
                JSONObject jsonProfile = new JSONObject(profile);
                String myName = jsonProfile.getString("hasName");
                String macAddress; ="";
                if (jsonProfile.has("macAddress")) {
                    macAddress = getMacAddressUpdated();
                }

                String reqName = json.getString("hasName");
                Log.d(TAG, " - Requested: " + reqName + "->" + macAddress);
                Log.d(TAG, " - I am: " + myName + "->" + myMacAddress);
                if (myName.equals(reqName))
                    Log.d(TAG, " - Its me!!");
                sendProfile();
                */

                    }

                    @Override
                    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

                    }
                });
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void startMyOwnForeground() {
        String NOTIFICATION_CHANNEL_ID = "es.unex.politecnica.spilab.csprofile";
        String channelName = "Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new
                NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle(this.getString(R.string.app_name))
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        Bundle extras = intent.getExtras();
        if (extras != null)
            profile = extras.getString("profile");
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartservice");
        broadcastIntent.setClass(this, Restarter.class);
        this.sendBroadcast(broadcastIntent);
    }

    private void setMessageNotification(@NonNull String topic, @NonNull String msg) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setContentTitle(topic)
                        .setContentText(msg);
        Intent resultIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(100, mBuilder.build());
    }

    private void subscribeTopic(Context ctx, String topic) {
        if (!topic.isEmpty()) {
            try {

                pahoMqttClient.unSubscribe(mqttAndroidClient, topic);
                pahoMqttClient.subscribe(mqttAndroidClient, topic, 1);

                Toast.makeText(ctx, "Subscribed to: " + topic, Toast.LENGTH_SHORT).show();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendProfile() {
        //Log.d(TAG," - Sending..."+profile);
        if (profile.equals("")) {
            Toast.makeText(this, "The profile is empty. Nothing to send.", Toast.LENGTH_SHORT).show();
            Log.d(TAG, " - The profile is empty. Nothing to send.");
        } else {
            Log.d(TAG, " - REQ: Sending profile to " + Constants.MQTT_BROKER_URL);
            try {
                //Constants.MQTT_BROKER_URL = "tcp://" + txtServerIp.getText().toString() + ":1883";
                //Log.d(TAG, "IP=" + Constants.MQTT_BROKER_URL);
                //pahoMqttClient.publishMessage(mqttAndroidClient, profile, 1, Constants.PUBLISH_TOPIC);

//                Intent intent = new Intent(ctx, MqttMessageService.class);
//                ctx.startService(intent);

                //Toast.makeText(ctx, "Profile sent successfully.", Toast.LENGTH_SHORT).show();
                //Log.d(TAG, " - REQ: Profile sent successfully.");
            } catch (Exception e) {
                //Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                //Log.d(TAG, " - REQ: Error sending the profile: " + e.getMessage());
            }
        }
    }

    private void readFileExternalStorage(File myExternalFile) {
        try {
            //File myExternalFile = new File(getExternalFilesDir("data"), "profile.json");
            FileInputStream iStream = new FileInputStream(myExternalFile + "/profile.json");

            //InputStream iStream = getApplicationContext().getAssets().open("profile.json");
            byte[] buffer = new byte[iStream.available()];
            iStream.read(buffer);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            byteStream.write(buffer);
            byteStream.close();
            iStream.close();
            profile = byteStream.toString();
            Log.d(TAG, " - File LOADED successfully: " + profile);
            //Toast.makeText(MainActivity.this," - Profile LOADED successfully").show();
        } catch (IOException e) {
            Log.d(TAG, " - Error LOADING profile");
            //writeFileExternalStorage(myExternalFile);
            //readFileExternalStorage(myExternalFile);
            e.printStackTrace();
        }
    }

    private String getMacAddressUpdated() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "";
    }

    private void readFileExternalStorageW3CTDE() {
        try {
            File myExternalFile = new File(Environment.getExternalStorageDirectory(), "W3CTDE-Droid");
            FileInputStream iStream = new FileInputStream(myExternalFile + "/profileW3CTDE.json");

            //Get Wifi ip
            //String ip = getIpAddress();
            //Log.d(TAG, " - IP: " + ip);

            //InputStream iStream = getApplicationContext().getAssets().open("profile.json");
            byte[] buffer = new byte[iStream.available()];
            iStream.read(buffer);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            byteStream.write(buffer);
            byteStream.close();
            iStream.close();
            profileW3CTD = byteStream.toString();
            Log.d(TAG, " - File LOADED successfully W3CTDE: " + profileW3CTD);

            // Fill fullProfile
            fullProfileW3CTDE = new ProfileW3CTDE();
            Gson gson = new Gson();
            fullProfileW3CTDE = gson.fromJson(profileW3CTD, ProfileW3CTDE.class);

            //writeFileExternalStorage();
            //writeFileExternalStorageW3CTDE();

            //Toast.makeText(MainActivity.this," - Profile LOADED successfully").show();
        } catch (Exception e) {
            Log.d(TAG, " - Error LOADING profile W3CTDE");
            //writeFileExternalStorageW3CTDE();
            readFileExternalStorageW3CTDE();
            e.printStackTrace();
        }
    }
}
