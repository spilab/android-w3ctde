package es.unex.politecnica.spilab.w3ctdedroid.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class ProfileW3CTDE {

    private String context = "";
    private String id = "";
    private String title;
    private String macAddress;

    //private List<Info> infos;
    private List<Info> securityDefinitions;
    private List<Info> securities;
    private List<Info> properties;
    private List<JsonObject> actions;
    private List<JsonObject> objectives;
    private List<JsonObject> situations;
    private List<Event> events;

    public ProfileW3CTDE() {
        this.context = "";
        this.id = "";
        this.title = "";
        this.macAddress = "";
        this.securityDefinitions = new ArrayList<Info>();
        this.securities = new ArrayList<Info>();
        this.properties = new ArrayList<Info>();
        this.actions = new ArrayList<JsonObject>();
        this.objectives = new ArrayList<JsonObject>();
        this.situations = new ArrayList<JsonObject>();
        this.events = new ArrayList<Event>();
    }

    public ProfileW3CTDE(String context, String id, String title, String macAddress, List<Info> securityDefinitions, List<Info> securities, List<Info> properties, List<JsonObject> actions, List<JsonObject> objectives, List<JsonObject> situations, List<Event> events) {
        this.context = context;
        this.id = id;
        this.title = title;
        this.macAddress = macAddress;
        this.securityDefinitions = securityDefinitions;
        this.securities = securities;
        this.properties = properties;
        this.actions = actions;
        this.objectives = objectives;
        this.situations = situations;
        this.events = events;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public List<Info> getSecurityDefinitions() {
        return securityDefinitions;
    }

    public void setSecurityDefinitions(List<Info> securityDefinitions) {
        this.securityDefinitions = securityDefinitions;
    }

    public List<Info> getSecurities() {
        return securities;
    }

    public void setSecurities(List<Info> securities) {
        this.securities = securities;
    }

    public List<Info> getProperties() {
        return properties;
    }

    public void setProperties(List<Info> properties) {
        this.properties = properties;
    }

    public List<JsonObject> getActions() {
        return actions;
    }

    public void setActions(List<JsonObject> actions) {
        this.actions = actions;
    }

    public List<JsonObject> getObjectives() {
        return objectives;
    }

    public void setObjectives(List<JsonObject> objectives) {
        this.objectives = objectives;
    }

    public List<JsonObject> getSituations() {
        return situations;
    }

    public void setSituations(List<JsonObject> situations) {
        this.situations = situations;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    /**
     * Create a new Action in JsonObject format
     *
     * @param name     name of the action
     * @param endpoint endpoint of the action
     * @return the created action
     */
    public JsonObject createActionObject(String name, String endpoint) {
        JsonObject href = new JsonObject();
        JsonArray jsa = new JsonArray();
        JsonObject js = new JsonObject();
        js.addProperty("href", endpoint);
        jsa.add(js);
        href.add("forms", jsa);
        JsonObject act = new JsonObject();
        act.add(name, href);

        return act;
    }

    /**
     * Create a new Objective in JsonObject format
     *
     * @param id       id
     * @param name     name
     * @param value    desired value
     * @param location location (lat, lng)
     * @param time     current time
     * @return the created bojective
     */
    public JsonObject createObjectiveObject(String id, String name, String value, String location, String time) {
        JsonObject obj = new JsonObject();

        obj.addProperty("id", id);
        obj.addProperty("name", name);
        obj.addProperty("value", value);

        JsonObject pro = new JsonObject();
        pro.addProperty("location", location);
        pro.addProperty("time", time);

        obj.add("properties", pro);

        return obj;
    }

    @Override
    public String toString() {
        return "ProfileW3CTDE{" +
                "context='" + context + '\'' +
                ", id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", macAddress='" + macAddress + '\'' +
                ", securityDefinitions=" + securityDefinitions +
                ", securities=" + securities +
                ", properties=" + properties +
                ", actions=" + actions +
                ", objectives=" + objectives +
                ", situations=" + situations +
                ", events=" + events +
                '}';
    }
}

