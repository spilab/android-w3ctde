package es.unex.politecnica.spilab.w3ctdedroid.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActionItem {
    private String name;
    private List<JsonArray> forms;

    public ActionItem() {
        this.name = "";
        this.forms = new ArrayList<>();
    }

    public ActionItem(String name, List<JsonArray> forms) {
        this.name = name;
        this.forms = forms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<JsonArray> getForms() {
        return forms;
    }

    public void setForms(List<JsonArray> forms) {
        this.forms = forms;
    }

    public List<JsonArray> createForms(String href) {
        List<JsonArray> forms = new ArrayList<JsonArray>();

        JsonObject json = new JsonObject();
        JsonArray jsa = new JsonArray();
        JsonObject js = new JsonObject();
        js.addProperty("href", href);
        jsa.add(js);
        json.add("forms", jsa);

        forms.add(jsa);

        return forms;
    }

    public String getHrefFromFrom() {
        String href = "";

        return href;
    }

    public String getHrefFromAction(String action) {
        String href = "";
        try {
            ArrayList formslist = new ArrayList();
            JSONObject obj = new JSONObject(action);
            JSONArray obja = (JSONArray) obj.get("forms");
            formslist.add(obja);
            JSONObject objh = new JSONObject(obja.getString(0));
            href = String.valueOf(objh.get("href"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(" - HREFFFFFF=" + href);

        return href;
    }

    public String getHrefFromForms() {
        String href = "";

        List<JsonArray> forms = this.getForms();
        for (JsonArray ja : forms) {
            try {
                JsonElement je = ja.get(0);
                JSONObject jo = new JSONObject(je.toString());
                href = jo.getString("href");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return href;
    }

    public ActionItem convertJsonObjectToActionItem(JsonObject js) {
        ActionItem ai = new ActionItem();

        return ai;
    }


    @Override
    public String toString() {
        return "ActionItem{" +
                "name='" + name + '\'' +
                ", forms=" + forms +
                '}';
    }
}
