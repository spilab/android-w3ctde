package es.unex.politecnica.spilab.w3ctdedroid.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.unex.politecnica.spilab.w3ctdedroid.MainActivity;
import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.adapters.CustomListAdapterWSituations;
import es.unex.politecnica.spilab.w3ctdedroid.models.SituationItem;

public class WSituationsFragment extends Fragment {

    private static ArrayList<SituationItem> situations;
    private static CustomListAdapterWSituations customListAdapterWSituations;
    private static ListView customListView;
    private static Context ctx;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_wsituations, viewGroup, false);

        customListView = (ListView) v.findViewById(R.id.custom_list_view_ws);
        registerForContextMenu(customListView);

        ctx = getContext();

        situations = new ArrayList<SituationItem>();


        List<JsonObject> profileSituations = new ArrayList<>(MainActivity.fullProfileW3CTDE.getSituations());

        for (JsonObject j : profileSituations) {
            //System.out.println("JS=" + j);

            Set<Map.Entry<String, JsonElement>> entrySet = j.entrySet();
            for (Map.Entry<String, JsonElement> entry : entrySet) {

                String key = entry.getKey();
                String value = entry.getValue().toString();

                System.out.println("JS-K=" + key);
                System.out.println("JS-V=" + value);

                try {
                    JSONObject jsonpr = new JSONObject(value);
                    String name = jsonpr.getString("name");

                    SituationItem si = new SituationItem();
                    si.setName(key + " - " + name);

                    List<Object> properties = new ArrayList<Object>();
                    properties.add(value);

                    si.setProperties(properties);

                    situations.add(si);

                    WRawDataFragment.reloadRawData();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }
        }

        customListAdapterWSituations = new CustomListAdapterWSituations(situations, getContext());
        customListView.setAdapter(customListAdapterWSituations);
        customListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(MainActivity.this, "Name : " + names[i] + "\n Profession : " + professions[i], Toast.LENGTH_SHORT).show();
            }
        });


        return v;
    }

    public static void reloadSituations() {
        situations = new ArrayList<SituationItem>();


        List<JsonObject> profileSituations = new ArrayList<>(MainActivity.fullProfileW3CTDE.getSituations());

        for (JsonObject j : profileSituations) {
            //System.out.println("JS=" + j);

            Set<Map.Entry<String, JsonElement>> entrySet = j.entrySet();
            for (Map.Entry<String, JsonElement> entry : entrySet) {

                String key = entry.getKey();
                String value = entry.getValue().toString();

                System.out.println("JS-K=" + key);
                System.out.println("JS-V=" + value);

                try {
                    JSONObject jsonpr = new JSONObject(value);
                    String name = jsonpr.getString("name");

                    SituationItem si = new SituationItem();
                    si.setName(key + " - " + name);

                    List<Object> properties = new ArrayList<Object>();
                    properties.add(value);

                    si.setProperties(properties);

                    situations.add(si);

                    WRawDataFragment.reloadRawData();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            }
        }

        customListAdapterWSituations = new CustomListAdapterWSituations(situations, ctx);
        customListView.setAdapter(customListAdapterWSituations);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.custom_list_view_ws) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_situation, menu);
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.delete_s:
                SituationItem si = situations.get(info.position);
                si = situations.get(info.position);
                String name = si.getName().split(" - ")[0];

                JsonObject jo = MainActivity.fullProfileW3CTDE.getSituations().get(0);
                jo.remove(name);

                customListAdapterWSituations.deleteSituation(info.position);
                MainActivity.writeFileExternalStorageW3CTDE();

                WRawDataFragment.reloadRawData();

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}