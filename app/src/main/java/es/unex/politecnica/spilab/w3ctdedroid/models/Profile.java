package es.unex.politecnica.spilab.w3ctdedroid.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Profile {
    private List<Info> infos;
    private List<Goal> goals;
    private List<Skill> skills;

    public Profile() {
        this.infos = new ArrayList<>();
        this.goals = new ArrayList<>();
        this.skills = new ArrayList<>();
    }

    public Profile(List<Info> infos, List<Goal> goals, List<Skill> skills) {
        this.infos = infos;
        this.goals = goals;
        this.skills = skills;
    }

    public List<Info> getInfos() {
        return infos;
    }

    public void setInfos(List<Info> infos) {
        this.infos = infos;
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "infos=" + infos +
                ", goals=" + goals +
                ", skills=" + skills +
                '}';
    }

    public void converterStringToProfile(String profile) {
        try {
            JSONObject json = new JSONObject(profile);
            this.getInfos().add(new Info("hasId", json.getString("hasId")));
            this.getInfos().add(new Info("hasName", json.getString("hasName")));
            this.getInfos().add(new Info("IPAddress", json.getString("IPAddress")));
            this.getInfos().add(new Info("macAddress", json.getString("macAddress")));

            JSONArray goals = json.getJSONArray("Operation");
            for (int i = 0; i < goals.length(); i++) {
                String name = goals.getJSONObject(i).getString("hasName");
                String value = goals.getJSONObject(i).getString("powerValue");
                Goal g = new Goal(name, value);
                this.getGoals().add(g);
            }

            JSONArray skills = json.getJSONArray("Service");
            for (int i = 0; i < skills.length(); i++) {
                String name = skills.getJSONObject(i).getString("hasName");
                String value = skills.getJSONObject(i).getString("realStateValue");
                String endpoint = skills.getJSONObject(i).getString("hasAddress");
                Skill s = new Skill(name, value, endpoint);
                this.getSkills().add(s);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public JSONObject convertProfileToJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("hasId", this.getInfos().get(0).getValue());
            json.put("hasName", this.getInfos().get(1).getValue());
            json.put("IPAddress", this.getInfos().get(2).getValue());
            json.put("macAddress", this.getInfos().get(3).getValue());

            JSONArray goals = new JSONArray();
            for (Goal g : this.getGoals()) {
                JSONObject gj = new JSONObject();
                gj.put("hasName", g.getName());
                gj.put("powerValue", g.getDesiredValue());
                goals.put(gj);
            }

            json.put("Operation", goals);

            JSONArray skills = new JSONArray();
            for (Skill s : this.getSkills()) {
                JSONObject sj = new JSONObject();
                sj.put("hasName", s.getName());
                sj.put("hasAddress", s.getEndpoint());
                sj.put("realStateValue", s.getCurrentValue());
                skills.put(sj);
            }

            json.put("Service", skills);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}
