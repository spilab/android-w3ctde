package es.unex.politecnica.spilab.w3ctdedroid;

/**
 * Created by brijesh on 20/4/17.
 */

public class Constants {

    public static String MQTT_BROKER_URL = "tcp://192.168.0.101:1883";

    public static final String PUBLISH_TOPIC = "profile";

    public static String CLIENT_ID = "none";

    public static final String DEFAULT_PROFILE = "{\n" +
            "   \"hasId\":\"1\",\n" +
            "   \"hasName\":\"Username\",\n" +
            "   \"IPAddress\":\"0.0.0.0\",\n" +
            "   \"macAddress\":\"00:00:00:00:00\",\n" +
            "   \"Operation\":[\n" +
            "      {\n" +
            "         \"hasName\":\"g_light_mode\",\n" +
            "         \"powerValue\":\"on\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"hasName\":\"g_light_luminosity\",\n" +
            "         \"powerValue\":\"100\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"hasName\":\"g_light_rgb\",\n" +
            "         \"powerValue\":\"FFFFFF\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"hasName\":\"g_temp\",\n" +
            "         \"powerValue\":\"22,5\"\n" +
            "      }\n" +
            "   ],\n" +
            "   \"Service\":[\n" +
            "      {\n" +
            "         \"hasName\":\"sk_cleanac\",\n" +
            "         \"hasAddress\":\"...\",\n" +
            "         \"realStateValue\":\"off\"\n" +
            "      }\n" +
            "   ]\n" +
            "}";

    public static final String DEFAULT_PROFILEW3CTD = "{\n" +
            "  \"actions\":[\n" +
            "    {\n" +
            "      \"sk_cleanac\":{\n" +
            "        \"forms\":[\n" +
            "          {\n" +
            "            \"href\":\"https://192.168.0.102\"\n" +
            "          }\n" +
            "        ]\n" +
            "      },\n" +
            "      \"sk_tv\":{\n" +
            "        \"name\":\"\",\n" +
            "        \"forms\":[\n" +
            "          {\n" +
            "            \"href\":\"tv.com\"\n" +
            "          }\n" +
            "        ]\n" +
            "      }\n" +
            "    }\n" +
            "  ],\n" +
            "  \"context\":\"https://www.w3.org/2019/wot/td/v1\",\n" +
            "  \"events\":[\n" +
            "    \n" +
            "  ],\n" +
            "  \"id\":\"W3CTDE-Username\",\n" +
            "  \"macAddress\":\"5c:3:39:39:fb:88\",\n" +
            "  \"objectives\":[\n" +
            "    {\n" +
            "      \"g_light_mode\":{\n" +
            "        \"id\":\"g_light_mode\",\n" +
            "        \"name\":\"Light Mode\",\n" +
            "        \"value\":\"on\",\n" +
            "        \"properties\":{\n" +
            "          \"location\":\"39.4570601,-6.3835215\",\n" +
            "          \"time\":\"20:24:57 CET\"\n" +
            "        }\n" +
            "      },\n" +
            "      \"g_light_luminosity\":{\n" +
            "        \"id\":\"g_light_luminosity\",\n" +
            "        \"name\":\"Light level\",\n" +
            "        \"value\":\"100\",\n" +
            "        \"properties\":{\n" +
            "          \"location\":\"39.4570601,-6.3835215\",\n" +
            "          \"time\":\"20:24:57 CET\"\n" +
            "        }\n" +
            "      },\n" +
            "      \"g_light_rgb\":{\n" +
            "        \"id\":\"g_light_rgb\",\n" +
            "        \"name\":\"Light RGB\",\n" +
            "        \"value\":\"00ffff\",\n" +
            "        \"properties\":{\n" +
            "          \"location\":\"39.4570601,-6.3835215\",\n" +
            "          \"time\":\"20:24:57 CET\"\n" +
            "        }\n" +
            "      },\n" +
            "      \"g_sh_switch\":{\n" +
            "        \"id\":\"g_sh_switch\",\n" +
            "        \"name\":\"Switch status\",\n" +
            "        \"value\":\"off\",\n" +
            "        \"properties\":{\n" +
            "          \"location\":\"39.4570601,-6.3835215\",\n" +
            "          \"time\":\"20:24:57 CET\"\n" +
            "        }\n" +
            "      }\n" +
            "    }\n" +
            "  ],\n" +
            "  \"properties\":[\n" +
            "    \n" +
            "  ],\n" +
            "  \"securities\":[\n" +
            "    \n" +
            "  ],\n" +
            "  \"securityDefinitions\":[\n" +
            "    \n" +
            "  ],\n" +
            "  \"situations\":[\n" +
            "    {\n" +
            "      \"lab\":{\n" +
            "        \"id\":\"lab\",\n" +
            "        \"name\":\"Laboratory\",\n" +
            "        \"properties\":{\n" +
            "          \"location\":\"39.4570601,-6.3835215\",\n" +
            "          \"date\":\"2012-12-03\",\n" +
            "          \"time\":\"15:05:00\",\n" +
            "          \"luminosity\":\"7\",\n" +
            "          \"weather\":\"sunny\"\n" +
            "        },\n" +
            "        \"objectives\":{\n" +
            "          \"type\":\"array\",\n" +
            "          \"items\":[\n" +
            "            {\n" +
            "              \"thing\":\"W3CTDE-Claudia\",\n" +
            "              \"objective\":\"g_sh_switch\",\n" +
            "              \"value\":\"off\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"thing\":\"W3CTDE-Paul\",\n" +
            "              \"objective\":\"g_light_luminosity\",\n" +
            "              \"value\":\"60\"\n" +
            "            }\n" +
            "          ]\n" +
            "        },\n" +
            "        \"things\":{\n" +
            "          \"type\":\"array\",\n" +
            "          \"items\":[\n" +
            "            \"W3CTDE-Daniel\",\n" +
            "            \"W3CTDE-Claudia\",\n" +
            "            \"W3CTDE-Paul\",\n" +
            "            \"W3CTDE-Yeelight\",\n" +
            "            \"W3CTDE-Shelly10x001\"\n" +
            "          ]\n" +
            "        },\n" +
            "        \"strategy\":{\n" +
            "          \"type\":\"array\",\n" +
            "          \"items\":[\n" +
            "            {\n" +
            "              \"thing\":\"W3CTDE-Yeelight\",\n" +
            "              \"action\":\"sk_light_luminosity\",\n" +
            "              \"value\":\"10\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"thing\":\"W3CTDE-Yeelight\",\n" +
            "              \"action\":\"sk_light_mode\",\n" +
            "              \"value\":\"on\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"thing\":\"W3CTDE-Yeelight\",\n" +
            "              \"action\":\"sk_light_rgb\",\n" +
            "              \"value\":\"0000ff\"\n" +
            "            },\n" +
            "            {\n" +
            "              \"thing\":\"W3CTDE-Shelly10x001\",\n" +
            "              \"action\":\"sk_sh_switch\",\n" +
            "              \"value\":\"on\"\n" +
            "            }\n" +
            "          ]\n" +
            "        }\n" +
            "      }\n" +
            "    }\n" +
            "  ],\n" +
            "  \"title\":\"Username\"\n" +
            "}";
}

