package es.unex.politecnica.spilab.w3ctdedroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.models.Info;

public class CustomListAdapterInfo extends BaseAdapter implements View.OnClickListener {
    private ArrayList<Info> infos;
    private Context context;

    public CustomListAdapterInfo(ArrayList<Info> infos, Context context) {
        this.infos = infos;
        this.context = context;
    }

    @Override
    public int getCount() {
        return infos.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.custom_list_view_infos_layout, null);
        ImageView option;

        Info goal = infos.get(i);
        //option = (ImageView) view.findViewById(R.id.goalOption);
        TextView key = (TextView) view.findViewById(R.id.key);
        TextView value = (TextView) view.findViewById(R.id.value);
        key.setText(goal.getKey());
        value.setText(goal.getValue());
        //option.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        /*int pos = view.getId();
        switch (view.getId()) {
            case R.id.goalOption:
                showPopupMenu(view, pos);
                break;
        }*/
    }

    // getting the popup menu
    private void showPopupMenu(View view, final int pos) {
        PopupMenu popupMenu = new PopupMenu(context, view);
        popupMenu.getMenuInflater().inflate(R.menu.option_menu, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.edit:

                        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
                        Toast.makeText(context, "Edit !" + info.position, Toast.LENGTH_SHORT).show();
                        notifyDataSetChanged();

                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    public void addInfo(Info g) {
        infos.add(g);
        notifyDataSetChanged();
    }

    public void editInfo(Info g, int pos) {
        infos.get(pos).setKey(g.getKey());
        infos.get(pos).setValue(g.getValue());
        notifyDataSetChanged();
    }

    public void deleteInfo(int pos) {
        infos.remove(pos);
        notifyDataSetChanged();
    }

    //file search result
    public void filterResult(ArrayList<Info> newInfos) {
        infos = new ArrayList<>();
        infos.addAll(infos);
        notifyDataSetChanged();
    }

}
