package es.unex.politecnica.spilab.w3ctdedroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.models.ObjectiveItem;

public class CustomListAdapterWObjectives extends BaseAdapter implements View.OnClickListener {
    private ArrayList<ObjectiveItem> objectives;
    private Context context;

    public CustomListAdapterWObjectives(ArrayList<ObjectiveItem> objectives, Context context) {
        this.objectives = objectives;
        this.context = context;
    }

    @Override
    public int getCount() {
        return objectives.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.custom_list_view_wobjectives_layout, null);
        ImageView option;


        ObjectiveItem oi = objectives.get(i);
        //option = (ImageView) view.findViewById(R.id.goalOption);
        TextView id = (TextView) view.findViewById(R.id.woname);
        TextView value = (TextView) view.findViewById(R.id.wodesiredValue);

        List<Object> properties = oi.getProperties();


        JSONObject obj = null;
        String val = "";
        try {
            obj = new JSONObject(properties.get(0).toString());
            val = (String) obj.get("value");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        id.setText(oi.getName());
        value.setText(val);


        //option.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        /*int pos = view.getId();
        switch (view.getId()) {
            case R.id.goalOption:
                showPopupMenu(view, pos);
                break;
        }*/
    }

    // getting the popup menu
    private void showPopupMenu(View view, final int pos) {
        PopupMenu popupMenu = new PopupMenu(context, view);
        popupMenu.getMenuInflater().inflate(R.menu.option_menu, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.edit:

                        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();

                        Toast.makeText(context, "Edit !" + info.position, Toast.LENGTH_SHORT).show();
                        //skills.remove(pos);
                        notifyDataSetChanged();

                        return true;
                    case R.id.remove:
                        Toast.makeText(context, "Remove !" + pos, Toast.LENGTH_SHORT).show();
                        //skills.remove(pos);
                        notifyDataSetChanged();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    public void addObjective(ObjectiveItem o) {
        objectives.add(o);
        notifyDataSetChanged();
    }

    public void deleteObjective(int pos) {
        objectives.remove(pos);
        notifyDataSetChanged();
    }


    public void editObjective(ObjectiveItem oi, int pos) {
        try {
            JSONObject ob = new JSONObject(oi.getProperties().get(0).toString());
            objectives.get(pos).setName(ob.getString("id"));
            objectives.get(pos).setProperties(oi.getProperties());
            notifyDataSetChanged();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /*

    //file search result
    public void filterResult(ArrayList<Skill> newSkills) {
        skills = new ArrayList<>();
        skills.addAll(skills);
        notifyDataSetChanged();
    }

     */

}
