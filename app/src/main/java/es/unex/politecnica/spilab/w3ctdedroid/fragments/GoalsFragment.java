package es.unex.politecnica.spilab.w3ctdedroid.fragments;

import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import es.unex.politecnica.spilab.w3ctdedroid.MainActivity;
import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.adapters.CustomListAdapterGoal;
import es.unex.politecnica.spilab.w3ctdedroid.models.Goal;

public class GoalsFragment extends Fragment {

    private ArrayList<Goal> goals;
    private CustomListAdapterGoal customListAdapterGoal;
    private ListView customListView;
    private FloatingActionButton fab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_goals, viewGroup, false);

        customListView = (ListView) v.findViewById(R.id.custom_list_view_g);
        registerForContextMenu(customListView);
        goals = new ArrayList<>(MainActivity.fullProfile.getGoals());
        customListAdapterGoal = new CustomListAdapterGoal(goals, getContext());
        customListView.setAdapter(customListAdapterGoal);
        //getDatas();
        customListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(MainActivity.this, "Name : " + names[i] + "\n Profession : " + professions[i], Toast.LENGTH_SHORT).show();
            }
        });

        Toast.makeText(getContext(), "Goals!", Toast.LENGTH_SHORT).show();

        // floating button
        fab = v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dialog add goal
                final View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_goal, null);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setView(v);
                builder1.setCancelable(true);
                builder1.setTitle("New goal");
                final EditText nameText = v.findViewById(R.id.nameText);
                final EditText valueText = v.findViewById(R.id.valueText);
                builder1.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String name = nameText.getText().toString();
                                String desiredValue = valueText.getText().toString();
                                Goal g = new Goal(name, desiredValue);
                                customListAdapterGoal.addGoal(g);

                                MainActivity.fullProfile.getGoals().add(g);
                                MainActivity.writeFileExternalStorage();

                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();


            }
        });


        return v;
    }

    // getting all the datas
    private void getDatas() {
        for (int i = 0; i < 5; i++) {
            //goals.add(new Goal(names[count], professions[count], photos[count]));
            goals.add(new Goal("Goal" + i, "value = " + i));

        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.custom_list_view_g) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_goal, menu);
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit_g:
                // Dialog add goal
                final View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_goal, null);
                //View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_new_goal, null);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setView(v);
                builder1.setCancelable(true);
                builder1.setTitle("Edit goal");

                String name = goals.get(info.position).getName();
                String desiredValue = goals.get(info.position).getDesiredValue();

                final EditText nameText = v.findViewById(R.id.nameText);
                nameText.setText(name);
                final EditText valueText = v.findViewById(R.id.valueText);
                valueText.setText(desiredValue);
                builder1.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String name = nameText.getText().toString();
                                String desiredValue = valueText.getText().toString();
                                Goal g = new Goal(name, desiredValue);
                                customListAdapterGoal.editGoal(g, info.position);

                                MainActivity.fullProfile.getGoals().get(info.position).setName(name);
                                MainActivity.fullProfile.getGoals().get(info.position).setDesiredValue(desiredValue);
                                MainActivity.writeFileExternalStorage();

                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
                return true;
            case R.id.delete_g:
                customListAdapterGoal.deleteGoal(info.position);
                MainActivity.fullProfile.getGoals().remove(info.position);
                MainActivity.writeFileExternalStorage();

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}