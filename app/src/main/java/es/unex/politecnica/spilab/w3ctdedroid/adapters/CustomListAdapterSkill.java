package es.unex.politecnica.spilab.w3ctdedroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.models.Skill;

public class CustomListAdapterSkill extends BaseAdapter implements View.OnClickListener {
    private ArrayList<Skill> skills;
    private Context context;

    public CustomListAdapterSkill(ArrayList<Skill> skills, Context context) {
        this.skills = skills;
        this.context = context;
    }

    @Override
    public int getCount() {
        return skills.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.custom_list_view_skills_layout, null);
        ImageView option;

        Skill skill = skills.get(i);
        //option = (ImageView) view.findViewById(R.id.goalOption);
        TextView name = (TextView) view.findViewById(R.id.name);
        TextView desiredValue = (TextView) view.findViewById(R.id.currentValue);
        TextView endpoint = (TextView) view.findViewById(R.id.endpoint);
        name.setText(skill.getName());
        desiredValue.setText(skill.getCurrentValue());
        endpoint.setText(skill.getEndpoint());
        //option.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        /*int pos = view.getId();
        switch (view.getId()) {
            case R.id.goalOption:
                showPopupMenu(view, pos);
                break;
        }*/
    }

    // getting the popup menu
    private void showPopupMenu(View view, final int pos) {
        PopupMenu popupMenu = new PopupMenu(context, view);
        popupMenu.getMenuInflater().inflate(R.menu.option_menu, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.edit:

                        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();

                        Toast.makeText(context, "Edit !" + info.position, Toast.LENGTH_SHORT).show();
                        //skills.remove(pos);
                        notifyDataSetChanged();

                        return true;
                    case R.id.remove:
                        Toast.makeText(context, "Remove !" + pos, Toast.LENGTH_SHORT).show();
                        //skills.remove(pos);
                        notifyDataSetChanged();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    public void addSkill(Skill s) {
        skills.add(s);
        notifyDataSetChanged();
    }

    public void editSkill(Skill s, int pos) {
        skills.get(pos).setName(s.getName());
        skills.get(pos).setCurrentValue(s.getCurrentValue());
        skills.get(pos).setEndpoint(s.getEndpoint());
        notifyDataSetChanged();
    }

    public void deleteSkill(int pos) {
        skills.remove(pos);
        notifyDataSetChanged();
    }

    //file search result
    public void filterResult(ArrayList<Skill> newSkills) {
        skills = new ArrayList<>();
        skills.addAll(skills);
        notifyDataSetChanged();
    }

}
