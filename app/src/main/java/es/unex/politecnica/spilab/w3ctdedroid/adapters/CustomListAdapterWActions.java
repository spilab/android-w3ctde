package es.unex.politecnica.spilab.w3ctdedroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.models.ActionItem;

public class CustomListAdapterWActions extends BaseAdapter implements View.OnClickListener {
    private ArrayList<ActionItem> actions;
    private Context context;

    public CustomListAdapterWActions(ArrayList<ActionItem> actions, Context context) {
        this.actions = actions;
        this.context = context;
    }

    @Override
    public int getCount() {
        return actions.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.custom_list_view_wactions_layout, null);
        ImageView option;

        ActionItem action = actions.get(i);
        //option = (ImageView) view.findViewById(R.id.goalOption);
        TextView id = (TextView) view.findViewById(R.id.waname);
        TextView href = (TextView) view.findViewById(R.id.wavalue);

        try {

            JSONArray array = new JSONArray(action.getForms().get(0).toString());
            JSONObject obj = (JSONObject) array.get(0);

            href.setText(obj.get("href").toString());
            id.setText(action.getName());

        } catch (Exception ex) {
            System.out.println(" - HREFE=" + ex);
        }

        //option.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        /*int pos = view.getId();
        switch (view.getId()) {
            case R.id.goalOption:
                showPopupMenu(view, pos);
                break;
        }*/
    }

    // getting the popup menu
    private void showPopupMenu(View view, final int pos) {
        PopupMenu popupMenu = new PopupMenu(context, view);
        popupMenu.getMenuInflater().inflate(R.menu.option_menu, popupMenu.getMenu());
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.edit:

                        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();

                        Toast.makeText(context, "Edit !" + info.position, Toast.LENGTH_SHORT).show();
                        //skills.remove(pos);
                        notifyDataSetChanged();

                        return true;
                    case R.id.remove:
                        Toast.makeText(context, "Remove !" + pos, Toast.LENGTH_SHORT).show();
                        //skills.remove(pos);
                        notifyDataSetChanged();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    public void addAction(ActionItem ai) {
        actions.add(ai);
        notifyDataSetChanged();
    }

    public void deleteAction(int pos) {
        actions.remove(pos);
        notifyDataSetChanged();
    }

    public void editAction(ActionItem ai, int pos) {
        actions.get(pos).setName(ai.getName());
        actions.get(pos).setForms(ai.getForms());
        notifyDataSetChanged();
    }


    /*
    //file search result
    public void filterResult(ArrayList<Skill> newSkills) {
        skills = new ArrayList<>();
        skills.addAll(skills);
        notifyDataSetChanged();
    }
    */


}
