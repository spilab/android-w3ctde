package es.unex.politecnica.spilab.w3ctdedroid.models;

import java.util.ArrayList;
import java.util.List;

public class Action {
    private List actionItems;

    public Action(){
        this.actionItems = new ArrayList();
    }

    public Action(List actionItems) {
        actionItems = actionItems;
    }

    public List getActionItems() {
        return actionItems;
    }

    public void setActionItems(List actionItems) {
        this.actionItems = actionItems;
    }

    @Override
    public String toString() {
        return "Action{" +
                "actionItems=" + actionItems +
                '}';
    }
}
