package es.unex.politecnica.spilab.w3ctdedroid.models;

import java.util.ArrayList;
import java.util.List;

public class SituationItem {
    private String name;
    private List<Object> properties;

    public SituationItem() {
        this.name = "";
        this.properties = new ArrayList<Object>();
    }

    public SituationItem(String name, List<Object> properties) {
        this.name = name;
        this.properties = properties;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getProperties() {
        return properties;
    }

    public void setProperties(List<Object> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "SituationItem{" +
                "name='" + name + '\'' +
                ", properties=" + properties +
                '}';
    }
}
