package es.unex.politecnica.spilab.w3ctdedroid.models;

import java.util.ArrayList;
import java.util.List;

public class Objective {
    private List<ObjectiveItem> objectives;

    private Objective() {
        this.objectives = new ArrayList<ObjectiveItem>();
    }

    public Objective(List<ObjectiveItem> objectives) {
        this.objectives = objectives;
    }

    public List<ObjectiveItem> getObjectives() {
        return objectives;
    }

    public void setObjectives(List<ObjectiveItem> objectives) {
        this.objectives = objectives;
    }

    @Override
    public String toString() {
        return "Objective{" +
                "objectives=" + objectives +
                '}';
    }
}
