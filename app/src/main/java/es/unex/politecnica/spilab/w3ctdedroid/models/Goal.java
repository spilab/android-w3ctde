package es.unex.politecnica.spilab.w3ctdedroid.models;

public class Goal {
    private String name;

    private String desiredValue;

    public Goal(String name, String desiredValue) {
        this.name = name;
        this.desiredValue = desiredValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesiredValue() {
        return desiredValue;
    }

    public void setDesiredValue(String desiredValue) {
        this.desiredValue = desiredValue;
    }

    @Override
    public String toString() {
        return "Goal{" +
                "name='" + name + '\'' +
                ", desiredValue='" + desiredValue + '\'' +
                '}';
    }
}
