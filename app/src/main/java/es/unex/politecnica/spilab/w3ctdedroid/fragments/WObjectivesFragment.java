package es.unex.politecnica.spilab.w3ctdedroid.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.unex.politecnica.spilab.w3ctdedroid.MainActivity;
import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.adapters.CustomListAdapterWObjectives;
import es.unex.politecnica.spilab.w3ctdedroid.models.ObjectiveItem;

public class WObjectivesFragment extends Fragment {

    private ArrayList<ObjectiveItem> objectives;
    private CustomListAdapterWObjectives customListAdapterWObjectives;
    private ListView customListView;
    private FloatingActionButton fab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_wobjectives, viewGroup, false);


        customListView = (ListView) v.findViewById(R.id.custom_list_view_wo);
        registerForContextMenu(customListView);
        objectives = new ArrayList<ObjectiveItem>();

        ArrayList<JsonObject> profileObjectives = new ArrayList<>(MainActivity.fullProfileW3CTDE.getObjectives());

        System.out.println(" - " + MainActivity.fullProfileW3CTDE);

        for (JsonObject j : profileObjectives) {
            //System.out.println("JS=" + j);

            Set<Map.Entry<String, JsonElement>> entrySet = j.entrySet();
            for (Map.Entry<String, JsonElement> entry : entrySet) {

                String key = entry.getKey();
                String value = entry.getValue().toString();

                //System.out.println(" - JO-K=" + key);
                //System.out.println(" - JO-V=" + value);

                try {

                    ObjectiveItem oi = new ObjectiveItem();
                    List<Object> properties = new ArrayList<Object>();
                    properties.add(value);

                    oi.setName(key);
                    oi.setProperties(properties);

                    objectives.add(oi);

                } catch (Exception ex) {
                    System.out.println(" - EX=" + ex);
                }
            }
        }


        customListAdapterWObjectives = new CustomListAdapterWObjectives(objectives, getContext());
        customListView.setAdapter(customListAdapterWObjectives);
        //getDatas();
        customListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(MainActivity.this, "Name : " + names[i] + "\n Profession : " + professions[i], Toast.LENGTH_SHORT).show();
            }
        });

        // floating button
        fab = v.findViewById(R.id.wo_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dialog add goal
                final View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_wobjective, null);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setView(v);
                builder1.setCancelable(true);
                builder1.setTitle("New objective");
                final EditText idText = v.findViewById(R.id.woidText);
                final EditText nameText = v.findViewById(R.id.wonameText);
                final EditText valueText = v.findViewById(R.id.wovalueText);
                final EditText locationText = v.findViewById(R.id.wolocationText);
                final EditText timeText = v.findViewById(R.id.wotimeText);
                builder1.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {


                                try {

                                    String oId = idText.getText().toString();
                                    String oName = nameText.getText().toString();
                                    String oDesiredValue = valueText.getText().toString();
                                    String oLocation = locationText.getText().toString();
                                    String oTime = timeText.getText().toString();

                                    JsonObject obj = MainActivity.fullProfileW3CTDE.createObjectiveObject(oId, oName, oDesiredValue, oLocation, oTime);
                                    MainActivity.fullProfileW3CTDE.getObjectives().get(0).add(oId, obj);

                                    ObjectiveItem oi = new ObjectiveItem();
                                    oi.setName(oName);

                                    JSONObject oiDetails = new JSONObject();
                                    oiDetails.put("name", oName);
                                    oiDetails.put("id", oId);
                                    oiDetails.put("value", oDesiredValue);
                                    oiDetails.put("location", oLocation);
                                    oiDetails.put("time", oTime);

                                    List<Object> properties = new ArrayList<Object>();
                                    properties.add(oiDetails.toString());
                                    oi.setProperties(properties);

                                    customListAdapterWObjectives.addObjective(oi);

                                    MainActivity.writeFileExternalStorageW3CTDE();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }



                                /*
                                JsonObject o = new JsonObject();
                                o.addProperty("id", "oi111");
                                o.addProperty("name", "on111");
                                o.addProperty("value", "ov111");

                                JsonObject pro = new JsonObject();
                                pro.addProperty("location", "ol111");
                                pro.addProperty("time", "ot111");

                                o.add("properties", pro);

                                MainActivity.fullProfileW3CTDE.getObjectives().get(0).add("oi111", o);

                                System.out.println(MainActivity.fullProfileW3CTDE);


                                 */

                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();


            }
        });


        return v;
    }

    // getting all the datas
    private void getDatas() {
        for (int i = 0; i < 5; i++) {
            //goals.add(new Goal(names[count], professions[count], photos[count]));
            //goals.add(new Goal("Goal" + i, "value = " + i));

        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.custom_list_view_wo) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_objective, menu);
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit_o:


                // Dialog add goal
                final View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_wobjective, null);
                //View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_new_goal, null);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setView(v);
                builder1.setCancelable(true);
                builder1.setTitle("Edit objective");

                ObjectiveItem oi = objectives.get(info.position);

                System.out.println(" - OB=" + oi);

                final EditText idText = v.findViewById(R.id.woidText);
                final EditText nameText = v.findViewById(R.id.wonameText);
                final EditText valueText = v.findViewById(R.id.wovalueText);
                final EditText locationText = v.findViewById(R.id.wolocationText);
                final EditText timeText = v.findViewById(R.id.wotimeText);

                try {


                    System.out.println(" - WOI=" + oi);

                    JSONObject data = new JSONObject(oi.getProperties().get(0).toString());

                    String oId = data.getString("id");
                    String oValue = data.getString("value");
                    String oName = data.getString("name");

                    System.out.println(" - OB=" + oId);

                    JSONObject properties = new JSONObject(data.getString("properties").toString());

                    String oLocation = properties.getString("location");
                    String oTime = properties.getString("time");


                    idText.setText(oId);
                    idText.setEnabled(false);
                    nameText.setText(oName);
                    valueText.setText(oValue);
                    locationText.setText(oLocation);
                    timeText.setText(oTime);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                builder1.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String oId = idText.getText().toString();
                                String oName = nameText.getText().toString();
                                String oDesiredValue = valueText.getText().toString();
                                String oLocation = locationText.getText().toString();
                                String oTime = timeText.getText().toString();


                                try {
                                    JsonObject obj = MainActivity.fullProfileW3CTDE.createObjectiveObject(oId, oName, oDesiredValue, oLocation, oTime);
                                    MainActivity.fullProfileW3CTDE.getObjectives().get(0).add(oId, obj);

                                    ObjectiveItem oi = new ObjectiveItem();
                                    oi.setName(oId);

                                    JsonObject oiDetails = new JsonObject();
                                    oiDetails.addProperty("name", oName);
                                    oiDetails.addProperty("id", oId);
                                    oiDetails.addProperty("value", oDesiredValue);

                                    JsonObject pro = new JsonObject();
                                    pro.addProperty("location", oLocation);
                                    pro.addProperty("time", oTime);

                                    oiDetails.add("properties", pro);

                                    List<Object> properties = new ArrayList<Object>();
                                    properties.add(oiDetails.toString());
                                    oi.setProperties(properties);




                                    //System.out.println(" - OBID=" + oiDetails);
                                    //System.out.println(" - OBIDoi=" + oi);

                                    JsonObject jo = MainActivity.fullProfileW3CTDE.getObjectives().get(0);
                                    jo.add(oId, oiDetails);

                                    MainActivity.fullProfileW3CTDE.getObjectives().set(0, jo);

                                    customListAdapterWObjectives.editObjective(oi, info.position);

                                    MainActivity.writeFileExternalStorageW3CTDE();

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }


                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
                return true;
            case R.id.delete_o:
                oi = objectives.get(info.position);
                String name = oi.getName();

                JsonObject jo = MainActivity.fullProfileW3CTDE.getObjectives().get(0);
                jo.remove(name);

                customListAdapterWObjectives.deleteObjective(info.position);
                MainActivity.writeFileExternalStorageW3CTDE();

                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }
}