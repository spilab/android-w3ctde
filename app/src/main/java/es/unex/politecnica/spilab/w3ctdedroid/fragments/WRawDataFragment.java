package es.unex.politecnica.spilab.w3ctdedroid.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

import es.unex.politecnica.spilab.w3ctdedroid.MainActivity;
import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.adapters.CustomListAdapterGoal;
import es.unex.politecnica.spilab.w3ctdedroid.models.Goal;

public class WRawDataFragment extends Fragment {

    private ArrayList<Goal> goals;
    private CustomListAdapterGoal customListAdapterGoal;
    private ListView customListView;
    private FloatingActionButton fab;
    public static TextView rawData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_wrawdata, viewGroup, false);
        rawData = v.findViewById(R.id.txtRawData);
        try {
            Gson gson = new Gson();
            String jo = gson.toJson(MainActivity.fullProfileW3CTDE);
            JSONObject aux = new JSONObject(jo);
            rawData.setText(aux.toString(2));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return v;
    }

    public static void reloadRawData() {
        try {
            Gson gson = new Gson();
            String jo = gson.toJson(MainActivity.fullProfileW3CTDE);
            JSONObject aux = new JSONObject(jo);
            rawData.setText(aux.toString(2));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}