package es.unex.politecnica.spilab.w3ctdedroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.models.SituationItem;

public class CustomListAdapterWSituations extends BaseAdapter implements View.OnClickListener {
    private ArrayList<SituationItem> situations;
    private Context context;

    public CustomListAdapterWSituations(ArrayList<SituationItem> situations, Context context) {
        this.situations = situations;
        this.context = context;
    }

    @Override
    public int getCount() {
        return situations.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.custom_list_view_wsituations_layout, null);

        SituationItem si = situations.get(i);
        TextView id = (TextView) view.findViewById(R.id.sitname);
        id.setText(si.getName());

        return view;
    }

    @Override
    public void onClick(View view) {
        /*int pos = view.getId();
        switch (view.getId()) {
            case R.id.goalOption:
                showPopupMenu(view, pos);
                break;
        }*/
    }

    // getting the popup menu
    private void showPopupMenu(View view, final int pos) {

    }

    public void deleteSituation(int pos) {
        situations.remove(pos);
        notifyDataSetChanged();
    }

    public static void reloadSituations(){

    }

}
