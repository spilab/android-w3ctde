package es.unex.politecnica.spilab.w3ctdedroid.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import es.unex.politecnica.spilab.w3ctdedroid.MainActivity;
import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.adapters.CustomListAdapterWActions;
import es.unex.politecnica.spilab.w3ctdedroid.models.ActionItem;

public class WActionsFragment extends Fragment {

    private ArrayList<ActionItem> actions;
    private CustomListAdapterWActions customListAdapterWActions;
    private ListView customListView;
    private FloatingActionButton fab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_wactions, viewGroup, false);

        customListView = (ListView) v.findViewById(R.id.custom_list_view_wa);
        registerForContextMenu(customListView);

        //Toast.makeText(getContext(), "WActions!", Toast.LENGTH_LONG).show();

        actions = new ArrayList<ActionItem>();

        ArrayList<JsonObject> profileActions = new ArrayList<>(MainActivity.fullProfileW3CTDE.getActions());
        for (JsonObject j : profileActions) {
            //System.out.println("JS=" + j);

            Set<Map.Entry<String, JsonElement>> entrySet = j.entrySet();
            for (Map.Entry<String, JsonElement> entry : entrySet) {

                String key = entry.getKey();
                String value = entry.getValue().toString();

                System.out.println("JA-K=" + key);
                System.out.println("JA-V=" + value);

                try {

                    ActionItem ai = new ActionItem();
                    String href = ai.getHrefFromAction(value);

                    ai.setName(key);
                    ai.setForms(ai.createForms(href));

                    actions.add(ai);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }


        customListAdapterWActions = new CustomListAdapterWActions(actions, getContext());
        customListView.setAdapter(customListAdapterWActions);
        //getDatas();
        customListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(MainActivity.this, "Name : " + names[i] + "\n Profession : " + professions[i], Toast.LENGTH_SHORT).show();
            }
        });


        // floating button
        fab = v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dialog add skill
                final View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_waction, null);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setView(v);
                builder1.setCancelable(true);
                builder1.setTitle("New Action");
                final EditText nameText = v.findViewById(R.id.wanameText);
                final EditText endpointText = v.findViewById(R.id.waendpointText);
                builder1.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String name = nameText.getText().toString();
                                String endpoint = endpointText.getText().toString();

                                JsonObject act = MainActivity.fullProfileW3CTDE.createActionObject(name, endpoint);

                                JsonObject a = new JsonObject();
                                a.addProperty("name", "");

                                JsonArray jsa = new JsonArray();
                                JsonObject js = new JsonObject();
                                js.addProperty("href", endpoint);
                                jsa.add(js);

                                a.add("forms", jsa);

                                MainActivity.fullProfileW3CTDE.getActions().get(0).add(name, a);

                                ActionItem ai = new ActionItem();
                                ai.setName(name);
                                ai.setForms(ai.createForms(endpoint));

                                customListAdapterWActions.addAction(ai);

                                MainActivity.writeFileExternalStorageW3CTDE();

                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();


            }
        });


        return v;
    }

    // getting all the datas
    private void getDatas() {
        for (int i = 0; i < 5; i++) {
            //skills.add(new Skill(names[count], professions[count], photos[count]));
            //skills.add(new Skill("Skill" + i, "value = " + i, "endp = " + i));

        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.custom_list_view_wa) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_action, menu);
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit_a:
                // Dialog add skill
                final View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_waction, null);
                //View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_new_goal, null);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setView(v);
                builder1.setCancelable(true);
                builder1.setTitle("Edit Action");

                ActionItem ai = actions.get((info.position));

                String name = ai.getName();

                final String endpointValue = ai.getHrefFromForms();

                final EditText nameText = v.findViewById(R.id.wanameText);
                nameText.setText(name);
                nameText.setEnabled(false);
                final EditText endpointText = v.findViewById(R.id.waendpointText);
                endpointText.setText(endpointValue);

                final String originalName = ai.getName();


                builder1.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String name = nameText.getText().toString();
                                String endpointValue = endpointText.getText().toString();

                                ActionItem ai = new ActionItem();
                                ai.setName(name);
                                ai.setForms(ai.createForms(endpointValue));

                                customListAdapterWActions.editAction(ai, info.position);

                                //JsonObject jo = MainActivity.fullProfileW3CTDE.getActions().get(info.position);

                                JsonObject jo = MainActivity.fullProfileW3CTDE.getActions().get(0);
                                System.out.println(" - JOO=" + jo.get(originalName));


                                //
                                JsonObject href = new JsonObject();
                                JsonArray jsa = new JsonArray();


                                JsonObject js = new JsonObject();
                                js.addProperty("href", endpointValue);

                                jsa.add(js);


                                href.add("forms", jsa);

                                JsonObject act = new JsonObject();
                                jo.add(originalName, href);


                                //

                                MainActivity.fullProfileW3CTDE.getActions().set(0, jo);


                                //MainActivity.fullProfileW3CTDE.getActions().set(info.position, act);

                                //MainActivity.fullProfileW3CTDE.getActions().get(0).getAsJsonObject().

                                MainActivity.writeFileExternalStorageW3CTDE();

                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });


                AlertDialog alert11 = builder1.create();
                alert11.show();


                return true;
            case R.id.delete_a:

                ai = actions.get((info.position));
                name = ai.getName();

                JsonObject jo = MainActivity.fullProfileW3CTDE.getActions().get(0);
                jo.remove(name);

                customListAdapterWActions.deleteAction(info.position);
                MainActivity.writeFileExternalStorageW3CTDE();

                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }
}