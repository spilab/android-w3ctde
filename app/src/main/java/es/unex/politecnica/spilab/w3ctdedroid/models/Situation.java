package es.unex.politecnica.spilab.w3ctdedroid.models;

import java.util.List;

public class Situation {
    private List situationsItems;

    public Situation(List situationsItems) {
        this.situationsItems = situationsItems;
    }

    public List getSituationsItems() {
        return situationsItems;
    }

    public void setSituationsItems(List situationsItems) {
        this.situationsItems = situationsItems;
    }

    @Override
    public String toString() {
        return "Situation{" +
                "situationsItems=" + situationsItems +
                '}';
    }
}
