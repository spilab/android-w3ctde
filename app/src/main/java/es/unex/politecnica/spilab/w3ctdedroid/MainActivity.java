package es.unex.politecnica.spilab.w3ctdedroid;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.unex.politecnica.spilab.w3ctdedroid.adapters.TabsAdapter;
import es.unex.politecnica.spilab.w3ctdedroid.models.Profile;
import es.unex.politecnica.spilab.w3ctdedroid.models.ProfileW3CTDE;
import es.unex.politecnica.spilab.w3ctdedroid.services.MqttMessageService;

public class MainActivity extends AppCompatActivity {

    //    private static MqttAndroidClient client;
    private static String TAG = "MainActivity";
//    private static PahoMqttClient pahoMqttClient;

//    private EditText textMessage, subscribeTopic, unSubscribeTopic, txtServerIp;
//    private Button publishMessage, subscribe, unSubscribe, btnSendProfile, btnSetIp;


    private static Context ctx;

    private static MainActivity main;

    public static Profile fullProfile;
    public static String profile;

    public static ProfileW3CTDE fullProfileW3CTDE;
    public static String profileW3CTD;
    private static String serverIp;

    private Intent mServiceIntent;

    //    private static boolean subscribed = false;
    private boolean storagePermissionsGranted = false;
    public static View v;

    private static final int STORAGE_PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        v = findViewById(android.R.id.content);

        ctx = getApplicationContext();
        main = this;

        // Get permissions
        getStoragePermission();

        //Test profile W3CTDE
        //testW3CTDEProfile();

    }

    private void loadContent() {
        // Read profile
        fullProfile = new Profile();
        Log.d(TAG, " - Storage: " + storagePermissionsGranted);
        if (storagePermissionsGranted) {
            // Read profile (first, check if exists)
            //File f = new File(getExternalFilesDir("data"), "profile.json");
            File f = new File(Environment.getExternalStorageDirectory(), "W3CTDE-Droid");
            checkFileExist(f);
            //readFileExternalStorage();
            readFileExternalStorageW3CTDE();

            // Get device mac address
            try {
                //JSONObject jsonProfile = new JSONObject(profile);
                JSONObject jsonProfileW3CTDE = new JSONObject(profileW3CTD);
                try {
                    //String macAddress = jsonProfile.getString("macAddress");
                    String macAddressW3CTD = jsonProfileW3CTDE.getString("macAddress");
                    Log.d(TAG, " - MAC-Profile:" + macAddressW3CTD);
                    //profile = jsonProfile.toString();
                    profileW3CTD = jsonProfileW3CTDE.toString();
                } catch (Exception e) {
                    String macAddressW3CTD = getMacAddressUpdated();
                    jsonProfileW3CTDE.put("macAddress", macAddressW3CTD);
                    Log.d(TAG, " - MAC-Device:" + macAddressW3CTD);
                    profileW3CTD = jsonProfileW3CTDE.toString();

                    //f = new File(Environment.getExternalStorageDirectory(), "W3CTDE-Droid");
                    //writeFileExternalStorage();
                    //readFileExternalStorage();

                    writeFileExternalStorageW3CTDE();
                    readFileExternalStorageW3CTDE();

                    e.printStackTrace();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d(TAG, " - Profile with mac: " + profile);

        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Setup"));
        /*
        tabLayout.addTab(tabLayout.newTab().setText("Info"));
        tabLayout.addTab(tabLayout.newTab().setText("Goals"));
        tabLayout.addTab(tabLayout.newTab().setText("Skills"));
         */
        tabLayout.addTab(tabLayout.newTab().setText("Info"));
        tabLayout.addTab(tabLayout.newTab().setText("Actions"));
        tabLayout.addTab(tabLayout.newTab().setText("Objectives"));
        tabLayout.addTab(tabLayout.newTab().setText("Situations"));
        tabLayout.addTab(tabLayout.newTab().setText("Raw data"));


        System.out.println(" - FPRW=" + MainActivity.fullProfileW3CTDE);


        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(tabsAdapter);
        //viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
        });

        startService();
    }

    private void startService() {
        serverIp = Constants.MQTT_BROKER_URL.split("//")[1].split(":")[0];

        // Stopping service if running
        MqttMessageService service = new MqttMessageService();
        mServiceIntent = new Intent(this, service.getClass());

        mServiceIntent.putExtra("profile", profile);

        boolean run = isMyServiceRunning(service.getClass());
        Log.d(TAG, " - Run1: " + run);
        if (!isMyServiceRunning(service.getClass())) {
            //mServiceIntent.putExtra("profile", profile);
            startService(mServiceIntent);
        }
        Log.d(TAG, " - Run1: " + run);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service status", "Running");
                return true;
            }
        }
        Log.i("Service status", "Not running");
        return false;
    }

    private static String getIpAddress() {
        WifiManager wifiManager = (WifiManager) ctx.getSystemService(WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();

        // Convert little-endian to big-endianif needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        String ipAddressString;
        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException ex) {
            Log.e("WIFIIP", "Unable to get host address.");
            ipAddressString = null;
        }

        return ipAddressString;
    }

    private static String getMacAddressUpdated() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "";
    }

    private void checkFileExist(File f) {
        if (!f.exists()) {
            Log.d(TAG, " - No existe la carpeta. Creando carpeta W3CTDE-Droid");
            f.mkdir();
            writeFileExternalStorage();
            writeFileExternalStorageW3CTDE();
        } else {
            Log.d(TAG, " - La carpeta existe");
        }
    }

    private void getStoragePermission() {
        Log.d(TAG, "getStoragePermission: getting storage permissions");
        String[] permissionsStorage = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_WIFI_STATE};
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED) {
            storagePermissionsGranted = true;
            loadContent();
        } else {
            ActivityCompat.requestPermissions(this,
                    permissionsStorage,
                    STORAGE_PERMISSION_REQUEST_CODE);
        }
    }

    private void testW3CTDEProfile() {
        fullProfileW3CTDE = new ProfileW3CTDE();
        // Info
        fullProfileW3CTDE.setContext("https://www.w3.org/2019/wot/td/v1");
        fullProfileW3CTDE.setId("WoTDaniel-1234");
        fullProfileW3CTDE.setTitle("daniel");
        fullProfileW3CTDE.setMacAddress("5c:3:39:39:fb:88");

        // Security definitions


        // Security


        // Properties


        // Actions

        //JsonObject actions = new JsonObject();
        List<JsonObject> actions = new ArrayList<JsonObject>();
        try {

            JsonObject href = new JsonObject();
            JsonArray jsa = new JsonArray();


            JsonObject js = new JsonObject();
            js.addProperty("href", "https://192.168.0.103");

            jsa.add(js);


            href.add("forms", jsa);

            JsonObject act = new JsonObject();
            act.add("sk_cleanac", href);

            JsonObject forms1 = new JsonObject();
            List<JSONObject> formsObject = new ArrayList<JSONObject>();

            actions.add(act);

            fullProfileW3CTDE.setActions(actions);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //fullProfileW3CTDE.setActions(actions);

        // Objectives
        //JsonObject objectives = new JsonObject();
        List<JsonObject> objectives = new ArrayList<JsonObject>();
        try {
            //
            JsonObject o = new JsonObject();
            o.addProperty("id", "g_light_mode");
            o.addProperty("name", "Light Mode");
            o.addProperty("value", "on");

            JsonObject pro = new JsonObject();
            pro.addProperty("location", "39.4570601,-6.3835215");
            pro.addProperty("time", "20:24:57 CET");

            o.add("properties", pro);

            //

            JsonObject obj = new JsonObject();
            obj.add("g_light_mode", o);

            o = new JsonObject();
            o.addProperty("id", "g_light_luminosity");
            o.addProperty("name", "Light level");
            o.addProperty("value", "100");

            pro = new JsonObject();
            pro.addProperty("location", "39.4570601,-6.3835215");
            pro.addProperty("time", "20:24:57 CET");

            o.add("properties", pro);
            obj.add("g_light_luminosity", o);

            //

            o = new JsonObject();
            o.addProperty("id", "g_light_rgb");
            o.addProperty("name", "Light RGB");
            o.addProperty("value", "00ffff");

            pro = new JsonObject();
            pro.addProperty("location", "39.4570601,-6.3835215");
            pro.addProperty("time", "20:24:57 CET");

            o.add("properties", pro);
            obj.add("g_light_rgby", o);

            //

            o = new JsonObject();
            o.addProperty("id", "g_temp");
            o.addProperty("name", "Temperature");
            o.addProperty("value", "22,5");

            pro = new JsonObject();
            pro.addProperty("location", "39.4570601,-6.3835215");
            pro.addProperty("time", "20:24:57 CET");

            o.add("properties", pro);
            obj.add("g_temp", o);

            //

            o = new JsonObject();
            o.addProperty("id", "g_sh_switch");
            o.addProperty("name", "Switch status");
            o.addProperty("value", "off");

            pro = new JsonObject();
            pro.addProperty("location", "39.4570601,-6.3835215");
            pro.addProperty("time", "20:24:57 CET");

            o.add("properties", pro);
            obj.add("g_sh_switch", o);

            //


            // Add objectives to JSON
            objectives.add(obj);
            //

        } catch (Exception e) {
            e.printStackTrace();
        }

        fullProfileW3CTDE.setObjectives(objectives);


        // Situations
        //JsonObject situations = new JsonObject();
        List<JsonObject> situations = new ArrayList<JsonObject>();
        try {
            // Info
            JsonObject s = new JsonObject();
            s.addProperty("id", "lab");
            s.addProperty("name", "Laboratory");

            // Properties
            JsonObject pro = new JsonObject();
            pro.addProperty("location", "39.4570601,-6.3835215");
            pro.addProperty("date", "2012-12-03");
            pro.addProperty("time", "15:05:00");
            pro.addProperty("luminosity", "7");
            pro.addProperty("weather", "sunny");

            s.add("properties", pro);
            JsonObject obj = new JsonObject();
            obj.add("lab", s);

            // Objectives
            JsonObject o = new JsonObject();
            o.addProperty("type", "array");

            // Ojbectives-items
            JsonArray jso = new JsonArray();
            JsonObject ite = new JsonObject();

            ite.addProperty("thing", "WoTClaudia-1234");
            ite.addProperty("objective", "g_sh_switch");
            ite.addProperty("value", "off");
            jso.add(ite);

            ite = new JsonObject();
            ite.addProperty("thing", "WoTPaul-1234");
            ite.addProperty("objective", "g_light_luminosity");
            ite.addProperty("value", "60");
            jso.add(ite);

            o.add("items", jso);
            s.add("objectives", o);

            // Things
            JsonObject t = new JsonObject();
            t.addProperty("type", "array");

            JsonArray jst = new JsonArray();
            jst.add("WoTDaniel-1234");
            jst.add("WoTClaudia-1234");
            jst.add("WoTPaul-1234");
            jst.add("WoTYeelight-1234");
            jst.add("WoTShelly10x001-1234");

            t.add("items", jst);
            s.add("things", t);

            // Strategy
            JsonObject str = new JsonObject();
            str.addProperty("type", "array");

            JsonArray jss = new JsonArray();
            JsonObject site = new JsonObject();
            site.addProperty("thing", "WoTYeelight-1234");
            site.addProperty("action", "sk_light_luminosity");
            site.addProperty("value", "10");
            jss.add(site);

            site = new JsonObject();
            site.addProperty("thing", "WoTYeelight-1234");
            site.addProperty("action", "sk_light_mode");
            site.addProperty("value", "off");
            jss.add(site);

            site = new JsonObject();
            site.addProperty("thing", "WoTYeelight-1234");
            site.addProperty("action", "sk_light_rgb");
            site.addProperty("value", "0000ff");
            jss.add(site);

            site = new JsonObject();
            site.addProperty("thing", "WoTShelly10x001-1234");
            site.addProperty("action", "sk_light_luminosity");
            site.addProperty("value", "on");
            jss.add(site);

            str.add("items", jss);
            s.add("strategy", str);


            // Add objectives to JSON
            situations.add(s);

            //

        } catch (Exception e) {
            e.printStackTrace();
        }

        fullProfileW3CTDE.setSituations(situations);

        // Events


        // Print profile
        Gson gson = new Gson();
        String json = gson.toJson(fullProfileW3CTDE);

        //Log.d(TAG, "W3CTDE-Profile=" + fullProfileW3CTDE);
        Log.d(TAG, "W3CTDEJSON-Profile=" + json);
        writeFileExternalStorageW3CTDE();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        storagePermissionsGranted = false;

        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            storagePermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            closeNow("You must grant the permissions");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    storagePermissionsGranted = true;
                    loadContent();


//                    File myExternalFile = new File(Environment.getExternalStorageDirectory(), "W3CTDE-Droid");
//                    if (!myExternalFile.exists()) {
//                        Log.d(TAG, " - No existe la carpeta...");
//                        myExternalFile.mkdir();
//                    }

                    //readFileExternalStorage(myExternalFile);
                    //writeFileExternalStorage(myExternalFile);


                }
            }

        }
    }

    private void closeNow(String message) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            finishAffinity();
        } else {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private static void reload() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            main.finishAffinity();
            main.startActivity(main.getIntent());
        } else {
            main.finish();
            main.startActivity(main.getIntent());
        }

    }

    private static void setUsername() {
        // Dialog add goal
        final View v = LayoutInflater.from(main).inflate(R.layout.dialog_username, null);
        //View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_new_goal, null);

        AlertDialog.Builder builder1 = new AlertDialog.Builder(main);
        builder1.setView(v);
        builder1.setCancelable(true);
        builder1.setTitle("Name");

        final EditText nameText = v.findViewById(R.id.nameText);
        builder1.setPositiveButton(
                "Save",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String name = nameText.getText().toString();
                        if (name.equals(""))
                            name = "Username";

                        MainActivity.fullProfile.getInfos().get(1).setValue(name);
                        MainActivity.writeFileExternalStorage();

                        MainActivity.fullProfileW3CTDE.setTitle(name);
                        MainActivity.fullProfileW3CTDE.setId("W3CTDE-" + name);
                        MainActivity.writeFileExternalStorageW3CTDE();

                        reload();
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void readFileExternalStorage() {
        try {
            File myExternalFile = new File(Environment.getExternalStorageDirectory(), "W3CTDE-Droid");
            FileInputStream iStream = new FileInputStream(myExternalFile + "/profile.json");

            //Get Wifi ip
            String ip = getIpAddress();
            Log.d(TAG, " - IP: " + ip);

            //InputStream iStream = getApplicationContext().getAssets().open("profile.json");
            byte[] buffer = new byte[iStream.available()];
            iStream.read(buffer);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            byteStream.write(buffer);
            byteStream.close();
            iStream.close();
            profile = byteStream.toString();
            Log.d(TAG, " - File LOADED successfully: " + profile);

            // Fill fullProfile
            fullProfile = new Profile();
            fullProfile.converterStringToProfile(profile);
            fullProfile.getInfos().get(2).setValue(ip);
            writeFileExternalStorage();
            writeFileExternalStorageW3CTDE();

            //Toast.makeText(MainActivity.this," - Profile LOADED successfully").show();
        } catch (Exception e) {
            Log.d(TAG, " - Error LOADING profile");
            writeFileExternalStorage();
            readFileExternalStorage();
            e.printStackTrace();
        }
    }

    public static void writeFileExternalStorage() {
        try {
            File myExternalFile = new File(Environment.getExternalStorageDirectory(), "W3CTDE-Droid");
            FileOutputStream fos = new FileOutputStream(myExternalFile + "/profile.json");
            boolean newProfile = false;
            if (profile == null) {
                profile = Constants.DEFAULT_PROFILE;
                fullProfile = new Profile();
                fullProfile.converterStringToProfile(profile);
                String macAddress = getMacAddressUpdated();
                fullProfile.getInfos().get(3).setValue(macAddress);
                Log.d(TAG, " - Creating default profile... " + fullProfile);
                MainActivity.setUsername();
                newProfile = true;
            } else {
                if (profile.equals("")) {
                    profile = Constants.DEFAULT_PROFILE;
                    fullProfile = new Profile();
                    fullProfile.converterStringToProfile(profile);
                    String macAddress = getMacAddressUpdated();
                    fullProfile.getInfos().get(3).setValue(macAddress);
                    Log.d(TAG, " - Creating default profile... " + fullProfile);
                    setUsername();
                    newProfile = true;
                }
            }
            //fos.write(profile.getBytes());

            JSONObject jsonProfile = fullProfile.convertProfileToJson();
            Log.d(TAG, " - 2FullProfile: " + jsonProfile.toString());


            fos.write(jsonProfile.toString().getBytes());
            fos.close();
            Log.d(TAG, " - File WRITED successfully");

            Snackbar.make(v, "Profile saved successfully!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        } catch (IOException e) {
            Snackbar.make(v, "Error saving the profile: " + e.getMessage(), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            Log.d(TAG, " - Error WRITING file");
            e.printStackTrace();
        }
    }

    public static void readFileExternalStorageW3CTDE() {
        try {
            File myExternalFile = new File(Environment.getExternalStorageDirectory(), "W3CTDE-Droid");
            FileInputStream iStream = new FileInputStream(myExternalFile + "/profileW3CTDE.json");

            //Get Wifi ip
            String ip = getIpAddress();
            Log.d(TAG, " - IP: " + ip);

            //InputStream iStream = getApplicationContext().getAssets().open("profile.json");
            byte[] buffer = new byte[iStream.available()];
            iStream.read(buffer);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            byteStream.write(buffer);
            byteStream.close();
            iStream.close();
            profileW3CTD = byteStream.toString();
            Log.d(TAG, " - File LOADED successfully W3CTDE: " + profileW3CTD);

            // Fill fullProfile
            fullProfileW3CTDE = new ProfileW3CTDE();
            Gson gson = new Gson();
            fullProfileW3CTDE = gson.fromJson(profileW3CTD, ProfileW3CTDE.class);

            //writeFileExternalStorage();
            writeFileExternalStorageW3CTDE();

            //Toast.makeText(MainActivity.this," - Profile LOADED successfully").show();
        } catch (Exception e) {
            Log.d(TAG, " - Error LOADING profile W3CTDE");
            writeFileExternalStorageW3CTDE();
            readFileExternalStorageW3CTDE();
            e.printStackTrace();
        }
    }

    public static void writeFileExternalStorageW3CTDE() {
        try {
            File myExternalFile = new File(Environment.getExternalStorageDirectory(), "W3CTDE-Droid");
            FileOutputStream fos = new FileOutputStream(myExternalFile + "/profileW3CTDE.json");


            boolean newProfile = false;
            if (profileW3CTD == null) {
                profileW3CTD = Constants.DEFAULT_PROFILEW3CTD;
                fullProfileW3CTDE = new ProfileW3CTDE();

                Gson gson = new Gson();
                fullProfileW3CTDE = gson.fromJson(profileW3CTD, ProfileW3CTDE.class);

                /*
                fullProfile.converterStringToProfile(profile);
                String macAddress = getMacAddressUpdated();
                fullProfile.getInfos().get(3).setValue(macAddress);
                 */

                Log.d(TAG, " - Creating default profile... " + profileW3CTD);
                MainActivity.setUsername();
                newProfile = true;
            } else {
                if (profileW3CTD.equals("")) {
                    profileW3CTD = Constants.DEFAULT_PROFILEW3CTD;
                    fullProfileW3CTDE = new ProfileW3CTDE();

                    Gson gson = new Gson();
                    fullProfileW3CTDE = gson.fromJson(profileW3CTD, ProfileW3CTDE.class);

                    /*
                    fullProfile.converterStringToProfile(profile);
                    String macAddress = getMacAddressUpdated();
                    fullProfile.getInfos().get(3).setValue(macAddress);
                     */

                    Log.d(TAG, " - Creating default profile... " + fullProfileW3CTDE);
                    setUsername();
                    newProfile = true;
                }
            }

            Log.d(TAG, " - W3CTD - FullProfile: " + fullProfileW3CTDE);

            // Print profile
            Gson gson = new Gson();
            String json = gson.toJson(fullProfileW3CTDE);

            fos.write(json.toString().getBytes());
            fos.close();
            Log.d(TAG, " - W3CTD - File WRITED successfully");

            Snackbar.make(v, "W3CTD - Profile saved successfully!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        } catch (IOException e) {
            Snackbar.make(v, "W3CTD - Error saving the profile: " + e.getMessage(), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            Log.d(TAG, "W3CTD -  - Error WRITING file");
            e.printStackTrace();
        }
    }

    public static Context getAppContext() {
        return MainActivity.getAppContext();
    }


}