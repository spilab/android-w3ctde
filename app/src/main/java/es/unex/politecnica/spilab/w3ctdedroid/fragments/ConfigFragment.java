package es.unex.politecnica.spilab.w3ctdedroid.fragments;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import es.unex.politecnica.spilab.w3ctdedroid.Constants;
import es.unex.politecnica.spilab.w3ctdedroid.MainActivity;
import es.unex.politecnica.spilab.w3ctdedroid.R;
import es.unex.politecnica.spilab.w3ctdedroid.services.MqttMessageService;
import es.unex.politecnica.spilab.w3ctdedroid.services.PahoMqttClient;

/**
 * Created by tutlane on 09-01-2018.
 */

public class ConfigFragment extends Fragment {

    private static MqttAndroidClient client;
    private static String TAG = "ConfigFragment";
    private static PahoMqttClient pahoMqttClient;

    private EditText textMessage, subscribeTopic, unSubscribeTopic, txtServerIp;
    private Button publishMessage, subscribe, unSubscribe, btnSendProfile, btnSetIp;


    public static String profile;
    public static String profileW3CTDE;
    private static String serverIp;

    private Intent mServiceIntent;

    private static boolean subscribed = false;
    private boolean storagePermissionsGranted = false;

    public static boolean btnIp = false;

    private static final int STORAGE_PERMISSION_REQUEST_CODE = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_config, viewGroup, false);

        btnSendProfile = (Button) v.findViewById(R.id.btnSendProfile);
        if (!ConfigFragment.btnIp)
            btnSendProfile.setEnabled(false);
        btnSetIp = (Button) v.findViewById(R.id.btnSetIp);
        txtServerIp = (EditText) v.findViewById(R.id.txtServerIp);
        serverIp = Constants.MQTT_BROKER_URL.split("//")[1].split(":")[0];
        txtServerIp.setText(serverIp);

        //Toast.makeText(getContext(), "Config!", Toast.LENGTH_SHORT).show();

        pahoMqttClient = new PahoMqttClient();
        //client = pahoMqttClient.getMqttClient(getContext(), Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);

        Log.d(TAG, "Creo vista");

        btnSendProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Context ctx = getContext();
                //sendProfile(ctx);
                sendProfileW3CTDE(ctx);
            }
        });

        btnSetIp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.MQTT_BROKER_URL = "tcp://" + txtServerIp.getText().toString() + ":1883";
                //Constants.CLIENT_ID = MainActivity.fullProfile.getInfos().get(3).getValue();
                Constants.CLIENT_ID = MainActivity.fullProfileW3CTDE.getMacAddress();
                Log.d(TAG, "ID=" + Constants.CLIENT_ID);
                ConfigFragment.btnIp = true;
                btnSendProfile.setEnabled(true);
                pahoMqttClient = new PahoMqttClient();
                client = pahoMqttClient.getMqttClient(getContext(), Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);

                //MqttMessageService.subscribed = false;

                // Stopping service if running
                MqttMessageService service = new MqttMessageService();
                mServiceIntent = new Intent(getActivity(), service.getClass());
                boolean run = isMyServiceRunning(service.getClass());
                Log.d(TAG, " - Run: " + run);

                if (isMyServiceRunning(service.getClass())) {
//                    getActivity().stopService(mServiceIntent);
//                    mServiceIntent.putExtra("profile", profile);
//                    //getContext().startService(mServiceIntent);
//                    getActivity().startService(mServiceIntent);
//
//                    run = isMyServiceRunning(service.getClass());
//                    Log.d(TAG, " - RunE: " + run);

                    startService();

                }

                Toast.makeText(getContext(), "IP set", Toast.LENGTH_SHORT).show();
            }
        });


        getStoragePermission();
        Log.d(TAG, " - Storage: " + storagePermissionsGranted);
        if (storagePermissionsGranted) {
            // Read profile (first, check if exists)
            //File f = new File(getExternalFilesDir("data"), "profile.json");
            File f = new File(Environment.getExternalStorageDirectory(), "Hassio");
            checkFileExist(f);
            readFileExternalStorage(f);

            // Get device mac address
            try {
                JSONObject jsonProfile = new JSONObject(profile);
                try {
                    String macAddress = jsonProfile.getString("macAddress");
                    Log.d(TAG, " - MAC-Profile:" + macAddress);
                    profile = jsonProfile.toString();
                } catch (Exception e) {
                    String macAddress = getMacAddressUpdated();
                    jsonProfile.put("macAddress", macAddress);
                    Log.d(TAG, " - MAC-Device:" + macAddress);
                    profile = jsonProfile.toString();

                    f = new File(Environment.getExternalStorageDirectory(), "Hassio");
                    //writeFileExternalStorage(f);
                    //readFileExternalStorage(f);

                    MainActivity.readFileExternalStorageW3CTDE();

                    e.printStackTrace();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.d(TAG, " - Profile with mac: " + profile);

            MqttMessageService service = new MqttMessageService();
            mServiceIntent = new Intent(this.getActivity(), service.getClass());
            boolean run = isMyServiceRunning(service.getClass());
            if (!isMyServiceRunning(service.getClass())) {
//                mServiceIntent.putExtra("profile", profile);
//                this.getActivity().startService(mServiceIntent);
                //startService();
            }
            Log.d(TAG, " - Run init: " + run);

        }


        return v;
    }

    private void startService() {
        MqttMessageService service = new MqttMessageService();
        mServiceIntent = new Intent(getActivity(), service.getClass());
        if (isMyServiceRunning(service.getClass())) {
            getActivity().stopService(mServiceIntent);
        }

        serverIp = Constants.MQTT_BROKER_URL.split("//")[1].split(":")[0];

        // Stopping service if running
        service = new MqttMessageService();
        mServiceIntent = new Intent(getContext(), service.getClass());

        mServiceIntent.putExtra("profile", profile);

        boolean run = isMyServiceRunning(service.getClass());
        Log.d(TAG, " - Run1: " + run);
        if (!isMyServiceRunning(service.getClass())) {
            //mServiceIntent.putExtra("profile", profile);
            getActivity().startService(mServiceIntent);
        }
        Log.d(TAG, " - Run1: " + run);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service status", "Running");
                return true;
            }
        }
        Log.i("Service status", "Not running");
        return false;
    }

    private String getMacAddressUpdated() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
            //handle exception
        }
        return "";
    }

    private void checkFileExist(File f) {
        if (!f.exists()) {
            Log.d(TAG, " - No existe la carpeta. Creando carpeta Hassio");
            f.mkdir();
            writeFileExternalStorage(f);
        } else {
            Log.d(TAG, " - La carpeta existe");
        }
    }

    public static void sendProfile(Context ctx) {

        if (profile == null)
            profile = Constants.DEFAULT_PROFILE;

        if (profile.equals("")) {
            Toast.makeText(ctx, "The profile is empty. Nothing to send.", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "The profile is empty. Nothing to send.");
        } else {
            try {
                //Constants.MQTT_BROKER_URL = "tcp://" + txtServerIp.getText().toString() + ":1883";
                //Log.d(TAG, "IP=" + Constants.MQTT_BROKER_URL);
                //pahoMqttClient = new PahoMqttClient();
                //client = pahoMqttClient.getMqttClient(getApplicationContext(), Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);
                //profile = MainActivity.fullProfile.convertProfileToJson().toString();
                //pahoMqttClient.publishMessage(client, profile, 1, Constants.PUBLISH_TOPIC);
                //if (!subscribed)
                //subscribeTopic(ctx,"request");


                //Intent intent = new Intent(ctx, MqttMessageService.class);
                //ctx.startService(intent);

                //Toast.makeText(ctx, "Profile sent successfully.", Toast.LENGTH_SHORT).show();
                //Log.d(TAG, "Profile sent successfully.");
            } catch (Exception e) {
                Toast.makeText(ctx, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Error: " + e.getMessage());
            }
        }
    }


    public static void sendProfileW3CTDE(Context ctx) {

        if (profileW3CTDE == null)
            profileW3CTDE = Constants.DEFAULT_PROFILEW3CTD;

        if (profileW3CTDE.equals("")) {
            Toast.makeText(ctx, "The profile is empty. Nothing to send.", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "The profile is empty. Nothing to send.");
        } else {
            try {
                //Constants.MQTT_BROKER_URL = "tcp://" + txtServerIp.getText().toString() + ":1883";
                //Log.d(TAG, "IP=" + Constants.MQTT_BROKER_URL);
                //pahoMqttClient = new PahoMqttClient();
                //client = pahoMqttClient.getMqttClient(getApplicationContext(), Constants.MQTT_BROKER_URL, Constants.CLIENT_ID);

                Gson gson = new Gson();
                profileW3CTDE = gson.toJson(MainActivity.fullProfileW3CTDE);

                pahoMqttClient.publishMessage(client, profileW3CTDE, 1, Constants.PUBLISH_TOPIC);
                //if (!subscribed)
                //subscribeTopic(ctx,"request");

                //pahoMqttClient.subscribe(client, "situation", 1);


                Intent intent = new Intent(ctx, MqttMessageService.class);
                //ctx.startService(intent);

                Toast.makeText(ctx, "W3CTDE Profile sent successfully.", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "W3CTDE Profile sent successfully.");
            } catch (Exception e) {
                Toast.makeText(ctx, "W3CTDE Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "W3CTDE Error: " + e.getMessage());
            }
        }
    }

    private void readFileExternalStorage(File myExternalFile) {
        try {
            //File myExternalFile = new File(getExternalFilesDir("data"), "profile.json");
            FileInputStream iStream = new FileInputStream(myExternalFile + "/profile.json");

            //InputStream iStream = getApplicationContext().getAssets().open("profile.json");
            byte[] buffer = new byte[iStream.available()];
            iStream.read(buffer);
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            byteStream.write(buffer);
            byteStream.close();
            iStream.close();
            profile = byteStream.toString();
            Log.d(TAG, " - File LOADED successfully: " + profile);
            //Toast.makeText(MainActivity.this," - Profile LOADED successfully").show();
        } catch (IOException e) {
            Log.d(TAG, " - Error LOADING profile");
            writeFileExternalStorage(myExternalFile);
            readFileExternalStorage(myExternalFile);
            e.printStackTrace();
        }
    }

    public static void writeFileExternalStorage(File myExternalFile) {
        try {
            //File myExternalFile = new File(getExternalFilesDir("data"), "profile.json");
            FileOutputStream fos = new FileOutputStream(myExternalFile + "/profile.json");
            if (profile == null)
                profile = Constants.DEFAULT_PROFILE;
            else {
                if (profile.equals(""))
                    profile = Constants.DEFAULT_PROFILE;
            }
            fos.write(profile.getBytes());
            fos.close();
            Log.d(TAG, " - File WRITED successfully");
        } catch (IOException e) {
            Log.d(TAG, " - Error WRITING file");
            e.printStackTrace();
        }
    }

    private void getStoragePermission() {
        Log.d(TAG, "getStoragePermission: getting storage permissions");
        String[] permissionsStorage = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_WIFI_STATE};
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_WIFI_STATE) == PackageManager.PERMISSION_GRANTED) {
            storagePermissionsGranted = true;

        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    permissionsStorage,
                    STORAGE_PERMISSION_REQUEST_CODE);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        storagePermissionsGranted = false;

        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            storagePermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    storagePermissionsGranted = true;

                    File myExternalFile = new File(Environment.getExternalStorageDirectory(), "Hassio");
                    if (!myExternalFile.exists()) {
                        Log.d(TAG, " - No existe la carpeta...");
                        myExternalFile.mkdir();
                    }

                    //readFileExternalStorage(myExternalFile);
                    //writeFileExternalStorage(myExternalFile);


                }
            }
        }
    }

}