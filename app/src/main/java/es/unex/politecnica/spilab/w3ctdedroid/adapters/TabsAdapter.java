package es.unex.politecnica.spilab.w3ctdedroid.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import es.unex.politecnica.spilab.w3ctdedroid.fragments.ConfigFragment;
import es.unex.politecnica.spilab.w3ctdedroid.fragments.WActionsFragment;
import es.unex.politecnica.spilab.w3ctdedroid.fragments.WInfosFragment;
import es.unex.politecnica.spilab.w3ctdedroid.fragments.WObjectivesFragment;
import es.unex.politecnica.spilab.w3ctdedroid.fragments.WRawDataFragment;
import es.unex.politecnica.spilab.w3ctdedroid.fragments.WSituationsFragment;

/**
 * Created by tutlane on 19-12-2017.
 */

public class TabsAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public TabsAdapter(FragmentManager fm, int NoofTabs) {
        super(fm);
        this.mNumOfTabs = NoofTabs;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new Fragment();
        switch (position) {
            /*
            case 0:
                fragment = new ConfigFragment();
                break;
            case 1:
                fragment = new InfosFragment();
                break;
            case 2:
                fragment = new GoalsFragment();
                break;
            case 3:
                fragment = new SkillsFragment();
                break;
            case 4:
                fragment = new WInfosFragment();
                break;
            case 5:
                fragment = new WActionsFragment();
                break;
            case 6:
                fragment = new WObjectivesFragment();
                break;
            case 7:
                fragment = new WSituationsFragment();
                break;
            case 8:
                fragment = new WRawDataFragment();
                break;
             */
            case 0:
                fragment = new ConfigFragment();
                break;
            case 1:
                fragment = new WInfosFragment();
                break;
            case 2:
                fragment = new WActionsFragment();
                break;
            case 3:
                fragment = new WObjectivesFragment();
                break;
            case 4:
                fragment = new WSituationsFragment();
                break;
            case 5:
                fragment = new WRawDataFragment();
                break;

            default:
                fragment = null;
        }
        return fragment;
    }
}