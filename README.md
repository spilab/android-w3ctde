# Table of content #

[TOC]

## Information ##
**W3CTDE-Droid** manages entities' descriptions to be offered to a device controller in the environment by using MQTT protocol (Check the [HassW3CTDE](https://bitbucket.org/spilab/server-node-python-w3ctde/src) Readme.md).


| Info     | Actions    | Objectives | Situations     | Raw data  |
| --------|---------|-------|---------|-------|
| ![](https://bitbucket.org/spilab/android-w3ctde/raw/1a2ae34bbf4b96d005ae28aeac42d76f322ad221/images/Info.png){width=200 height=422}  | ![](https://bitbucket.org/spilab/android-w3ctde/raw/1a2ae34bbf4b96d005ae28aeac42d76f322ad221/images/Actions.png){width=200 height=422}   | ![](https://bitbucket.org/spilab/android-w3ctde/raw/1a2ae34bbf4b96d005ae28aeac42d76f322ad221/images/Objectives.png){width=200 height=422}     | ![](https://bitbucket.org/spilab/android-w3ctde/raw/1a2ae34bbf4b96d005ae28aeac42d76f322ad221/images/Situations.png){width=200 height=422}       | ![](https://bitbucket.org/spilab/android-w3ctde/raw/1a2ae34bbf4b96d005ae28aeac42d76f322ad221/images/RawData.png){width=200 height=422}    |

## Get started ##
Just clone the repository and compile the apk

`$ git clone https://dafloresm@bitbucket.org/spilab/android-w3ctde.git`

### Requirements ###
Smartphone with Android 8.0 or higher.

### Installation ###
 1. Compile and install the application.
 2. Run it!

## Usage ###
There are 4 main tabs:

 1. **Setup**: to set server information. Just configure the IP. Note: the smartphone and the server must be within the same network. In this tab the profile can also be set manually in case the MQTT server does not be able to request it.

 2. **Info**: to show the profile information such as the ID, name, IP and MAC addresses.

 3. **Actions**: to specify the different actions.

 4. **Objectives**: to specify the different objectives.

 5. **Situations**: to check/delete current situations.

 6. **Raw data**: to show the whole description in json format

# Contact #
dfloresm@unex.es